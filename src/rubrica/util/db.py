from sqlalchemy import create_engine, Column, Integer, Numeric, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

engine = create_engine('mysql+pymysql://marco:marco@server2008-21/rubrica')
Base = declarative_base()

class Contatti(Base):
    __tablename__ = 'contatti'
    id_contatto = Column(Integer, primary_key=True)
    nome = Column(String(50), index=True)
    note = Column(String(50))

class Numeri(Base):
    __tablename__ = 'numeri'
    id_numero = Column(Integer, primary_key=True)
    id_contatto = Column(Integer(), ForeignKey('contatti.id_contatto'))
    contatti = relationship("Contatti", backref=backref('numeri', order_by=id_numero))
    numero = Column(String(20))
    note = Column(String(50))