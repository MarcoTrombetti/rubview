"""pattern MVC - control"""
import wx
import wx.lib.mixins.inspection as wit
from controls import control01


class MyApp(wx.App, wit.InspectionMixin):
    """Classe che comprende l'ispettore di oggetti """
    def OnInit(self):
        """ inizializza l'ispettore """
        self.Init()  # initialize the inspection tool
        return True


if __name__ == "__main__":
    app = MyApp(False)
    frame = control01.ViewFrame(None)
    frame.Show(True)
    app.MainLoop()
